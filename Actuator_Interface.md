An actuator describes a single *physical actuator* instance with at max 1DoF and is strictly tight to a **Joint**.
It might hereby take a single command value for its appropriate mode of operation, such as a desired joint velocity or effort - in rare cases an actuator might actually take a precise joint position value.
The implementation of this actuator might then convert the desired value into PWM or other hardware specific commands and control the hardware.
Similarly, an actuator might provide state feedback.
Depending on the setup, the motor encoders might provide position, velocity, effort or current feedback.

The URDF snippet for an actuator might look like the following:
```xml
<ros2_control name="my_simple_servo_motor" type="actuator">
  <hardware>
    <class>simple_servo_motor_pkg/SimpleServoMotor</class>
    <param name="serial_port">/dev/tty0</param>
  ...
  </hardware>
  <joint name="joint1">
    <command_interface name="position">
      <param name="min">-1.57</param>
      <param name="max">1.57</param>
    </command>

    <state_interface name="position"/>
    ...
  </joint>
</ros2_control>
```
The snippet above depicts a simple hardware setup, with a single actuator which controls one logical joint.
The joint here is configured to be commanded in position values, whereas the state feedback is also position.

If a joint is configured with a command or state interface the hardware is not supporting, a runtime error shall occur during startup.
Opposite to it, a joint might be configured with only the minimal required interfaces even though the hardware might support additional interfaces (such as "current" or "voltage").
Those shall simply be not instantiated and thus ignored.