```xml
...
<joint name="my_joint">
  <command_interface name="joint_command_interface">
    <param name="my_joint_command_param">1.5</param>
  </command_interface>
  <state_interface name="joint_state_interface1" />
  <state_interface name="joint_state_interface2" />
</joint>
...
```
A sensor is a second logical component which represents an interface to a hardware resource which has read-only state feedback.
Similar to the Joint interface, the sensor interface shims over the physical hardware.