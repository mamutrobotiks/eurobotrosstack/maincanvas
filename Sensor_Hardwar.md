A sensor is a hardware component which only has state feedback.
It can be considered as a read-only hardware resource and thus does not require exclusive access management - that is it can be used by multiple controllers concurrently.
```xml
<ros2_control name="my_simple_sensor">
  <hardware type="sensor">
    <class>simple_sensor_pkg/SimpleSensor</class>
    <param name="serial_port">/dev/tty0</param>
    ...
  </hardware>
  <sensor name="my_sensor">
    <state_interface name="roll" />
    <state_interface name="pitch" />
    <state_interface name="yaw" />
  </sensor>
</ros2_control>
```
Note that we technically have a separation between a physical hardware resource (`<hardware type="sensor">`) and a logical component (`<sensor name="my_sensor">`), both called *Sensor*.
We don't individually specify them further in this document as they don't have a significant semantic interpretation for the user.