A joint is considered a logical component and is being actuated by at least one actuator (generally, it might be under- or over-actuated depending on the actual hardware setup).
The joint is meant to be abstraction layer between a controller instance and the underlaying hardware.
The abstraction is needed to shim over a potentially complex hardware setup in order to control a joint.
A single joint might be controlled by multiple motors with a non-trivial transmission interface, yet a controller only takes care about joint values.

A joint is configured in conjunction with a hardware resource such as **Actuator** or **System**.
The joint component is used through command and state interfaces which are declared in the URDF.
The command interfaces describe the value in which this joint can be controlled (e.g. effort or velocity) where as the state interfaces are considered read-only feedback interfaces.
The interfaces itself can be further specified by passing in parameters.
An example URDF:
```xml
...
<joint name="my_joint">
  <command_interface name="joint_command_interface">
    <param name="my_joint_command_param">1.5</param>
  </command_interface>
  <state_interface name="joint_state_interface1" />
  <state_interface name="joint_state_interface2" />
</joint>
...
```
